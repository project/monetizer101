Drupal integration for Monetizer 101 products see http://monetizer101.com/products/

# Dependencies
* Flag

# Installation
1. Download and enable the module.

2. Configure at Administer > Configuration > System > Monetizer101

3. Enable monetizer for each content type you want. Settings for each content type
available in content type setting form Administer > Structure > Content type > Content_type Name
