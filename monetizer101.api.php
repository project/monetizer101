<?php

/**
 * @file
 * Documents hooks provided by this module.
 */

/**
 * Alter the data before it gets to the page.
 *
 * @param array $data
 *   The data array
 */
function hook_monetizer101_data_alter(&$data) {
  $data['test'] = 'yes';
}
